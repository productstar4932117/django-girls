from python:3
env pythonunbuffered 1
workdir /code

copy . /code/

run pip install -r requirements.txt

volume ["/code/db"]
expose 8080
cmd sh init.sh && python3 manage.py runserver 0.0.0.0:8080
